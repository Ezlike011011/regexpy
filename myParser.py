import graphviz as gv

SYMBOLS = {'(':"LEFT_PAREN", ')':"RIGHT_PAREN", '\\':"BACKSLASH", '*':"STAR", '|':"PIPE"}
ALIASES = {'w':"abcdefghijklmnopqrstuvwxyz0123456789_", 'd':"0123456789", 's':"\n\t"}

class Token:
    def __init__(self, name, value = None):
        self.name = name
        self.value = value

    def __str__(self):
        return f"({self.name} : {self.value})"

class Lexer:
    def __init__(self, pattern):
        self.symbols = SYMBOLS
        self.pattern = pattern
        self.index = 0

    def getToken(self):
        if(self.index < len(self.pattern)):
            char = self.pattern[self.index]
            self.index += 1
            if(char in SYMBOLS.keys()):
                return Token(SYMBOLS[char], char)
            else:
                return Token("CHAR", char)
        else:
            return Token("EMPTY")

class parseError(Exception):
    def __init__(self, expectedName, parser):
        lexer = parser.lexer
        preString = ""
        if(lexer.index > 0):
            preString = lexer.pattern[:lexer.index - 1]

        postString = ""
        if(lexer.index < len(lexer.pattern)):
            postString = lexer.pattern[lexer.index:]

        atString = f"{preString} <{lexer.pattern[lexer.index - 1]}> {postString}"
        super().__init__(f"Expected {expectedName}, got {parser.currentToken.name}: at {atString}")

class Parser:
    def __init__(self, lexer = None, pattern = None):
        if(lexer):
            if(not pattern):
                self.lexer = lexer
            else:
                raise Exception("Lexer and Pattern passed to parser")
        else:
            if(pattern):
                self.lexer = Lexer(pattern)
            else:
                raise Exception("Neither Lexer nor pattern passed to parser")
        
        self.currentToken = self.lexer.getToken()
    
    def consume(self, name):
        #Sanity check to ensure the parser is on the right track
        if(self.currentToken.name == name):
            self.currentToken = self.lexer.getToken()
        else:
            raise parseError(name,self)

    def parse(self):
        result = self.expression()

        #Check that the whole expression was actually parsed
        if(self.currentToken.name != "EMPTY"):
            raise parseError("END OF EXPRESSION", self)
        
        return result


    '''
    Grammar:

    Ɛ Denotes empty string
    $ Denotes any symbol (used by language or otherwise)
    ₵ Denotes any character (as deemed by lexer)

    E : LEFT_PAREN E RIGHT_PAREN E O | LEFT_PAREN E RIGHT_PAREN STAR E O | W E O | Ɛ
    O : PIPE E | Ɛ
    W : L | LW | L STAR
    L : S | BACKSLASH $
    S : ₵
    '''
    
    def expression(self):
        #Since the expression is optional in the grammar (as shown by Ɛ),
        #   only parse an expression if one is there
        if(self.currentToken.name in ["LEFT_PAREN", "CHAR", "BACKSLASH"]):

            if(self.currentToken.name == "LEFT_PAREN"):
                self.consume("LEFT_PAREN")
                node = self.expression()
                self.consume("RIGHT_PAREN")

                #If there is a star, add it to the branch
                if(self.currentToken.name == "STAR"):
                    node = TreeNode(left = node, token = self.currentToken)
                    self.consume("STAR")

            else:
                #expression must be a word if it isn't wrapped in parentheses 
                node = self.word()

            

            #If there is an expression, make a concatination branch split
            if(expr := self.expression()):
                #Wanted to use the tie symbol but no charset support womp womp ⌢
                node = TreeNode(Token("CONCAT", "~"), node, expr)

            #If there is an operator, 
            if(op := self.operator(node)):
                node = op

            return node
            

    #This section of the grammar is implemented to remove the left recursion of E: E|E
    def operator(self, leftNode):
        #Since the operator is optional in the grammar (as shown by Ɛ),
        #   only parse an operator if one is there
        if(self.currentToken.name == "PIPE"):
            #Consume the pipe token to ensure the right expression is parsed correctly, but save it for the treenode
            pipeToken = self.currentToken
            self.consume("PIPE")

            node = TreeNode(token= pipeToken, left = leftNode, right = self.expression())
            
            return node

    
    def word(self):
        #Word must contain at least one literal, so that is parsed
        curr = self.literal()

        if(self.currentToken.name in ["CHAR", "BACKSLASH"]):
            curr = TreeNode(Token("CONCAT", '~'), curr, self.word())
        elif(self.currentToken.name == "STAR"):
            curr = TreeNode(left = curr, token = self.currentToken)
            self.consume("STAR")
            
        return curr


    def literal(self):
        if(self.currentToken.name == "CHAR"):
            return self.symbol()
        elif(self.currentToken.name == "BACKSLASH"):
            self.consume("BACKSLASH")

            #Backslash indicates an alias, but if it isn't escaping a symbol or an alias, is discarded
            if(self.currentToken.value in ALIASES.keys()):
                node = TreeNode(token = Token("ALIAS", f"\\{self.currentToken.value}"))
                self.consume("CHAR")
            elif(self.currentToken.value in SYMBOLS.keys()):
                #Backslash is escaping a symbol, create a char token for it
                node = TreeNode(token = Token("CHAR", self.currentToken.value))
                #Consume what ever symbol was escaped
                self.consume(self.currentToken.name)
            else:
                node = self.symbol()


            return node
        else:
            raise parseError("CHAR or BACKSLASH", self)

    def symbol(self):
        if(self.currentToken.name == "CHAR"):
            node = TreeNode(token = self.currentToken)
            self.consume("CHAR")
            return node
        else:
            raise parseError("CHAR", self)


class TreeNode:
    def __init__(self, token, left = None, right = None):
        self.left = left
        self.right = right
        self.token = token

    def generateImage(self, imageName = "out"):
        g = gv.Graph(filename=f"{imageName}.dot")
        self.addSubGraph(g, 0)
        g.format = "png"
        g.view(filename=imageName,cleanup=True)

    def addSubGraph(self, graph, uuid):
        thisUUID = uuid
        symbol = self.token.value
        if("\\" in symbol):
            symbol = symbol.replace("\\", "\\\\")
        graph.node(name=str(thisUUID),label=symbol)
        if(self.left):
            graph.edge(str(thisUUID), str(uuid + 1))
            uuid = self.left.addSubGraph(graph, uuid + 1)
        if(self.right):
            graph.edge(str(thisUUID), str(uuid + 1))
            uuid = self.right.addSubGraph(graph, uuid + 1)
        return uuid

    def __str__(self):
        lstr = ""
        rstr = ""
        if(self.left):
            lstr = str(self.left)
        if(self.right):
            rstr = str(self.right)

        return f"""{self.token}\n\t{lstr}\n\t{rstr}"""
