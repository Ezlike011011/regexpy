import graphviz as gv

#Custom error type, may flesh out later
class FSAFileError(Exception):
    pass

#Datatype for states, for ease of indexing
class State:
    def __init__(self, name, isFinal):
        self.name = name
        self.isFinal = isFinal

    def __str__(self):
        if(self.isFinal):
            return f"({self.name})"
        return str(self.name)

#Datatype for deltas, for ease of indexing
class Delta:
    def __init__(self, startState, symbol, nextState):
        self.startState = startState
        self.symbol = symbol
        self.nextState = nextState

    def __str__(self):
        return f"{self.startState}-{self.symbol}->{self.nextState}"
 
#Datatype for FSM, to contain methods required for operation
class NDFSM:
    def __init__(self,alphabet = "", states = [], deltas = [], start = "", epsilon = 'Ɛ'):
        self.alphabet = alphabet
        self.states = states
        self.deltas = deltas
        self.start = start
        self.epsilon = epsilon

    def addState(self, name, isFinal = False):
        self.states.append(s := State(name, isFinal))
        return s

    def removeState(self, state):
        self.states = [s for s in self.states if (s != state)]

    def addDelta(self, startState, symbol, endState):
        self.deltas.append(d := Delta(startState,symbol,endState))
        return d

    
    def execute(self, tape, state = None):
        """
        Execute this machine from {state} on {tape}
        
        Preconditions:
            * self.states must be populated with State objects
            * self.deltas must be populated with Delta objects
            * {tape} must be a list of comparable types to the types in self.states[n].symbol
        Parameters:
            * Positional:
                * tape : the sequence of symbols that this fsa should execute on
                * state : the State object to start execution from (defaults to self.start)
        Return: A boolean representing if, with the given {tape} from the given {state}, 
            any possible moves results in an empty tape in a final state 
        """
        
        #if this is the initial call, start at the start state
        if(state):
            state = self.start

        symbol = ""
        #The symbol is the next symbol on the tape
        if(len(tape) > 0):
            symbol = tape[0]
        #get all transitions from this state that follow the symbol or the epsilon move
        transitions = [d for d in self.deltas if (((d.symbol in symbol) or (d.symbol == self.epsilon)) and (d.startState == state))] 
        
        #base case, no more symbols on tape
        if((len(tape) == 0)):
            #if there are no epsilon transitions, this current state is the resting state
            if(len([d for d in transitions if (d.symbol == self.epsilon)]) == 0):
                return state.isFinal

        result = False
        for transition in transitions:  
            #pass full tape if moving along an epsilon move, otherwise consume character.
            result = result or self.execute(tape[(1 if (transition.symbol != self.epsilon) else 0):], transition.nextState)
        return result

    def removeExtraStates(self):
        newStates = []
        for s in self.states:
            keep = False
            for d in self.deltas:
                if(s in [d.startState, d.nextState]):
                    keep = True
                    break
            if(keep):
                newStates.append(s) 
        self.states = newStates


    def generateImage(self, imageName = "out", **kwargs):
        config = kwargs.keys()
        g = gv.Digraph(filename = f"{imageName}.dot")
        g.attr("graph", rankdir = "LR")
        g.attr("graph",  arrowtail="dot")

        #Add nodes for every state
        showLabels = ("showLabels" in config and kwargs["showLabels"])

        for state in self.states:
            if(not showLabels):
                g.attr("node", label = "")
            g.node(str(state.name), shape=("doublecircle" if state.isFinal else "circle"))

        #Combine labels of duplicate transitions
        if("combineTransitions" in config and kwargs["combineTransitions"]):
            transitionDict = {}
            for transition in self.deltas:
                key = (transition.startState.name, transition.nextState.name)
                if key not in transitionDict:
                    transitionDict[key] = [transition.symbol]
                else:
                    if(transition.symbol not in transitionDict[key]):
                        transitionDict[key].append(transition.symbol)

            for key,elem in transitionDict.items():
                label = ",".join(elem)
                g.edge(str(key[0]),str(key[1]),label)

        else:
            for delta in self.deltas:
                g.edge(str(delta.startState.name), str(delta.nextState.name), delta.symbol)

        if("leadingArrow" in config and kwargs["leadingArrow"]):
            g.node("", shape="none", height=".0", width=".0")
            g.edge("", str(self.start.name), arrowhead="vee")

        g.format="png"
        g.view(filename=imageName,cleanup=True)

    def __str__(self):
        return f"""
            alphabet: {self.alphabet}
            states: {[str(s) for s in self.states]} 
            deltas: {[str(d) for d in self.deltas]}
            start: {self.start}
            epsilon: {self.epsilon}
        """



#Helper function to check if the passed in state is in the passed fsm's states
def validState(fsm, state):
    names = [s.name for s in fsm.states]
    return (state in names)
 
#All prefixes to be used in the file
PREFIXES = ['E', 'A', 'S', 'B', 'D', 'T']

def parseFsaFile(fileName):
    if(not (fileName.endswith(".fsa") or fileName.endswith(".ndfsa"))):
        raise FSAFileError(f"File is not of correct type \"{fileName}\"")

    with open(fileName, "r+", encoding="utf8") as fsaFile:

        lines = fsaFile.readlines()

        #Make a new FSM
        fsm = NDFSM()

        for line in lines:
            #Seperate the body and prefix of the text
            prefix = line[0]
            body = line[2:-1]

            if(prefix not in PREFIXES):
                raise FSAFileError(f"Prefix \"{prefix}\" is not a valid prefix")

            if(prefix == 'E'):
                if(len(body) != 1):
                    raise FSAFileError(f"Improper epsilon character: {body}")
                if(fsm.epsilon != 'Ɛ'):
                    raise FSAFileError(f"Redefinition of epsilon character {body} (Previously defined to be \"{fsm.epsilon}\")")
                fsm.epsilon = body
            elif(prefix == 'A'):
                if(fsm.epsilon in body):
                    raise FSAFileError(f"Epsilon Character \"{fsm.epsilon}\" in alphabet \"{body}\"")
                if(fsm.alphabet != ""):
                    raise FSAFileError(f"Redefinition of alphabet {body} (Previously defined to be \"{fsm.alphabet}\")")
                fsm.alphabet = body
            elif(prefix == 'S'):
                #Get list of states/values
                states = body.split(',')

                #States are improperly formatted, not a multiple of 2.
                if(len(states)%2 != 0):
                    raise FSAFileError(f"States are improperly formatted: {body}")
                    
                else:
                    #Add all states to the FSM 
                    while((len(states) != 0)):
                        #duplicate state name
                        if(states[0] in [f.name for f in fsm.states]):
                            raise FSAFileError(f"State name already taken: {states[0]}")
                            
                        else:
                            fsm.states.append(State(states[0],states[1] == '1'))
                            states = states[2:]
            elif(prefix == 'B'):
                if(fsm.states == []):
                    raise FSAFileError(f"Start State Defined before States {body} (Need S lines before the B line)")

                if(body.count(',') != 0):
                    raise FSAFileError(f"Multiple start states defined: {body}")

                if(fsm.start != ""):
                    raise FSAFileError(f"Redefinition of Start State {body} (Previously defined to be \"{fsm.start}\")")

                if(not validState(fsm, body)):
                    raise FSAFileError(f"Declared start state \"{body}\" is not a previously declared state")
                    
                fsm.start = [state for state in fsm.states if(state.name == body)][0]
            
            elif(prefix == 'D'):
                #Get list of starts/symbols/nexts
                deltas = body.split(',')
                
                #Deltas are improperly formatted, should be in sets of 3
                if(len(deltas)%3 != 0):
                    raise FSAFileError(f"Deltas are improperly formatted {line}")
                    
                else:
                    while(len(deltas) != 0):
                        #if either state or the character are not valid states or symbols respectively, break out
                        if(not validState(fsm, deltas[0])):
                            raise FSAFileError(f"{deltas[0]} is not a valid state")

                        elif((deltas[1] != fsm.epsilon) and (deltas[1] not in fsm.alphabet)):
                            raise FSAFileError(f"{deltas[1]} is not in the alphabet for this machine \"{fsm.alphabet}\"")

                        elif(not validState(fsm, deltas[2])):
                            raise FSAFileError(f"{deltas[2]} is not a valid state")
                        
                        else:
                            start = [state for state in fsm.states if(state.name == deltas[0])][0]
                            end = [state for state in fsm.states if(state.name == deltas[2])][0]
                            fsm.deltas.append(Delta(start,deltas[1],end))
                            deltas = deltas[3:]

        if(fsm.alphabet == ""):
            raise FSAFileError("Alphabet not defined")

        if(fsm.states == []):
            raise FSAFileError("States not defined")

        if(fsm.start == ""):
            raise FSAFileError("Start state not defined")
    return fsm

def runFsaFile(fileName, fsm = ""):
    if(fsm == ""):
        fsm = parseFsaFile(fileName)

    #Get lines of file
    with open(fileName, "r+", encoding="utf8") as fsaFile:
        lines = fsaFile.readlines()
        for i,line in enumerate(lines):
            #Get the body of text, removing the prefix and the newline
            body = line[2:-1]
            if(line[0] == 'T'):
                lines[i] += f"O {'Accepted' if fsm.execute(body) else 'Rejected'}\n"

        fsaFile.seek(0)
        fsaFile.writelines(lines)


if (__name__ == "__main__"):
    fileName = input("What file should be run?")
    runFsaFile(fileName)