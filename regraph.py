import graphviz as gv

#Custom error type, may flesh out later
class FSAFileError(Exception):
    pass

#Datatype for states, for ease of indexing
class State:
    def __init__(self, name, isFinal):
        self.name = name
        self.isFinal = isFinal

    def __str__(self):
        if(self.isFinal):
            return f"({self.name})"
        return str(self.name)

#Datatype for deltas, for ease of indexing
class Delta:
    def __init__(self, startState, symbol, nextState):
        self.startState = startState
        self.symbol = symbol
        self.nextState = nextState

    def __str__(self):
        return f"{self.startState}-{self.symbol}->{self.nextState}"
 
#Datatype for FSM, to contain methods required for operation
class NDFSM:
    def __init__(self,alphabet = "", states = [], deltas = [], start = "", epsilon = 'Ɛ'):
        self.alphabet = alphabet
        self.states = states
        self.deltas = deltas
        self.start = start
        self.epsilon = epsilon

    def addState(self, name, isFinal = False):
        self.states.append(s := State(name, isFinal))
        return s

    def removeState(self, state):
        self.states = [s for s in self.states if (s != state)]

    def addDelta(self, startState, symbol, endState):
        self.deltas.append(d := Delta(startState,symbol,endState))
        return d

    def execute(self, tape, state = None):
        """
        Execute this FSM on the passed tape from the given state

        Preconditions:
            * "state" must be a State object that exists in self.states, if not provided, it becomes self.start
            * "tape" must be a string of characters, empty or not, comprised of characters contained in self.alphabet
            * self.deltas must be populated only with Delta objects
            * self.states must be populated only with State objects
        Return:
            * A boolean representing if, with the given tape, following any of the viable moves in the FSA results in an empty tape
                in an accepting state
        """
        
        #if this is the initial call, start at the start state
        if(state == None):
            state = self.start

        symbol = ""
        #The symbol is the next symbol on the tape
        if(len(tape) > 0):
            symbol = tape[0]
        #get all transitions from this state that follow the symbol or the epsilon move
        transitions = [d for d in self.deltas if (((d.symbol in symbol) or (d.symbol == self.epsilon)) and (d.startState == state))] 
        
        #base case, no more symbols on tape
        if((len(tape) == 0)):
            #if there are no epsilon transitions, this current state is the resting state
            if(len([d for d in transitions if (d.symbol == self.epsilon)]) == 0):
                return state.isFinal

        result = False
        for transition in transitions:  
            #pass full tape if moving along an epsilon move, otherwise consume character.
            result = result or self.execute(tape[(1 if (transition.symbol != self.epsilon) else 0):], transition.nextState)
        return result

    def generateImage(self, imageName = "fsm", **kwargs):
        """
        Generate a PNG image representative of this FSM

        Preconditions:
            * Graphviz must be configured properly 
            * self.deltas must be populated only with Delta objects
            * self.states must be populated only with State objects
        Postconditions:
            * an image (by default "fsm.png") is generated depicting the graph representation of the FSM
        Keyword Arguments:
            * imageName: A string containing the desired output filename ("{imageName}.png")
            * showLabels: A boolean representing if labels should be shown on the nodes of the graph
            * combineTransitions: A boolean representing if labels of transitions between the same two states should be combined
            * leadingArrow: A boolean representing if the graph should have an arrow leading into the starting state to differentiate it
        """
        config = kwargs.keys()
        g = gv.Digraph(filename = f"{imageName}.dot")
        g.attr("graph", rankdir = "LR")
        g.attr("graph",  arrowtail="dot")

        #Add nodes for every state
        showLabels = ("showLabels" in config and kwargs["showLabels"])

        for state in self.states:
            if(not showLabels):
                g.attr("node", label = "")
            g.node(str(state.name), shape=("doublecircle" if state.isFinal else "circle"))

        #Combine labels of duplicate transitions
        if("combineTransitions" in config and kwargs["combineTransitions"]):
            transitionDict = {}
            for transition in self.deltas:
                key = (transition.startState.name, transition.nextState.name)
                if key not in transitionDict:
                    transitionDict[key] = [transition.symbol]
                else:
                    if(transition.symbol not in transitionDict[key]):
                        transitionDict[key].append(transition.symbol)

            for key,elem in transitionDict.items():
                label = ",".join(elem)
                g.edge(str(key[0]),str(key[1]),label)

        else:
            for delta in self.deltas:
                g.edge(str(delta.startState.name), str(delta.nextState.name), delta.symbol)

        if(not ("leadingArrow" in config and not kwargs["leadingArrow"])):
            g.node("", shape="none", height=".0", width=".0")
            g.edge("", str(self.start.name), arrowhead="vee")

        g.format="png"
        g.view(filename=imageName,cleanup=True)

    def __str__(self):
        return f"""
            alphabet: {self.alphabet}
            states: {[str(s) for s in self.states]} 
            deltas: {[str(d) for d in self.deltas]}
            start: {self.start}
            epsilon: {self.epsilon}
        """

import graphviz as gv

SYMBOLS = {'(':"LEFT_PAREN", ')':"RIGHT_PAREN", '\\':"BACKSLASH", '*':"STAR", '|':"PIPE"}
ALIASES = {'w':"abcdefghijklmnopqrstuvwxyz0123456789_", 'd':"0123456789", 's':"\n\t"}

class Token:
    def __init__(self, name, value = None):
        self.name = name
        self.value = value

    def __str__(self):
        return f"({self.name} : {self.value})"

class Lexer:
    def __init__(self, pattern):
        self.symbols = SYMBOLS
        self.pattern = pattern
        self.index = 0

    def getToken(self):
        if(self.index < len(self.pattern)):
            char = self.pattern[self.index]
            self.index += 1
            if(char in SYMBOLS.keys()):
                return Token(SYMBOLS[char], char)
            else:
                return Token("CHAR", char)
        else:
            return Token("EMPTY")

class parseError(Exception):
    def __init__(self, expectedName, parser):
        lexer = parser.lexer
        preString = ""
        if(lexer.index > 0):
            preString = lexer.pattern[:lexer.index - 1]

        postString = ""
        if(lexer.index < len(lexer.pattern)):
            postString = lexer.pattern[lexer.index:]

        atString = f"{preString} <{lexer.pattern[lexer.index - 1]}> {postString}"
        super().__init__(f"Expected {expectedName}, got {parser.currentToken.name}: at {atString}")

class Parser:
    def __init__(self, lexer = None, pattern = None):
        if(lexer):
            if(not pattern):
                self.lexer = lexer
            else:
                raise Exception("Lexer and Pattern passed to parser")
        else:
            if(pattern):
                self.lexer = Lexer(pattern)
            else:
                raise Exception("Neither Lexer nor pattern passed to parser")
        
        self.currentToken = self.lexer.getToken()
    
    def consume(self, name):
        #Sanity check to ensure the parser is on the right track
        if(self.currentToken.name == name):
            self.currentToken = self.lexer.getToken()
        else:
            raise parseError(name,self)

    def parse(self):
        result = self.expression()

        #Check that the whole expression was actually parsed
        if(self.currentToken.name != "EMPTY"):
            raise parseError("END OF EXPRESSION", self)
        
        return result


    '''
    Grammar:

    Ɛ Denotes empty string
    $ Denotes any symbol (used by language or otherwise)
    ₵ Denotes any character (as deemed by lexer)

    E : LEFT_PAREN E RIGHT_PAREN E O | LEFT_PAREN E RIGHT_PAREN STAR E O | W E O | Ɛ
    O : PIPE E | Ɛ
    W : L | LW | L STAR
    L : S | BACKSLASH $
    S : ₵
    '''
    
    def expression(self):
        #Since the expression is optional in the grammar (as shown by Ɛ),
        #   only parse an expression if one is there
        if(self.currentToken.name in ["LEFT_PAREN", "CHAR", "BACKSLASH"]):

            if(self.currentToken.name == "LEFT_PAREN"):
                self.consume("LEFT_PAREN")
                node = self.expression()
                self.consume("RIGHT_PAREN")

                #If there is a star, add it to the branch
                if(self.currentToken.name == "STAR"):
                    node = TreeNode(left = node, token = self.currentToken)
                    self.consume("STAR")

            else:
                #expression must be a word if it isn't wrapped in parentheses 
                node = self.word()

            

            #If there is an expression, make a concatination branch split
            if(expr := self.expression()):
                #Wanted to use the tie symbol but no charset support womp womp ⌢
                node = TreeNode(Token("CONCAT", "~"), node, expr)

            #If there is an operator, 
            if(op := self.operator(node)):
                node = op

            return node
            

    #This section of the grammar is implemented to remove the left recursion of E: E|E
    def operator(self, leftNode):
        #Since the operator is optional in the grammar (as shown by Ɛ),
        #   only parse an operator if one is there
        if(self.currentToken.name == "PIPE"):
            #Consume the pipe token to ensure the right expression is parsed correctly, but save it for the treenode
            pipeToken = self.currentToken
            self.consume("PIPE")

            node = TreeNode(token= pipeToken, left = leftNode, right = self.expression())
            
            return node

    
    def word(self):
        #Word must contain at least one literal, so that is parsed
        curr = self.literal()

        if(self.currentToken.name in ["CHAR", "BACKSLASH"]):
            curr = TreeNode(Token("CONCAT", '~'), curr, self.word())
        elif(self.currentToken.name == "STAR"):
            curr = TreeNode(left = curr, token = self.currentToken)
            self.consume("STAR")
            
        return curr


    def literal(self):
        if(self.currentToken.name == "CHAR"):
            return self.symbol()
        elif(self.currentToken.name == "BACKSLASH"):
            self.consume("BACKSLASH")

            #Backslash indicates an alias, but if it isn't escaping a symbol or an alias, is discarded
            if(self.currentToken.value in ALIASES.keys()):
                node = TreeNode(token = Token("ALIAS", f"\\{self.currentToken.value}"))
                self.consume("CHAR")
            elif(self.currentToken.value in SYMBOLS.keys()):
                #Backslash is escaping a symbol, create a char token for it
                node = TreeNode(token = Token("CHAR", self.currentToken.value))
                #Consume what ever symbol was escaped
                self.consume(self.currentToken.name)
            else:
                node = self.symbol()


            return node
        else:
            raise parseError("CHAR or BACKSLASH", self)

    def symbol(self):
        if(self.currentToken.name == "CHAR"):
            node = TreeNode(token = self.currentToken)
            self.consume("CHAR")
            return node
        else:
            raise parseError("CHAR", self)


class TreeNode:
    def __init__(self, token, left = None, right = None):
        self.left = left
        self.right = right
        self.token = token

    def generateImage(self, imageName = "out"):
        g = gv.Graph(filename=f"{imageName}.dot")
        self.addSubGraph(g, 0)
        g.format = "png"
        g.view(filename=imageName,cleanup=True)

    def addSubGraph(self, graph, uuid):
        thisUUID = uuid
        symbol = self.token.value
        if("\\" in symbol):
            symbol = symbol.replace("\\", "\\\\")
        graph.node(name=str(thisUUID),label=symbol)
        if(self.left):
            graph.edge(str(thisUUID), str(uuid + 1))
            uuid = self.left.addSubGraph(graph, uuid + 1)
        if(self.right):
            graph.edge(str(thisUUID), str(uuid + 1))
            uuid = self.right.addSubGraph(graph, uuid + 1)
        return uuid

    def __str__(self):
        lstr = ""
        rstr = ""
        if(self.left):
            lstr = str(self.left)
        if(self.right):
            rstr = str(self.right)

        return f"""{self.token}\n\t{lstr}\n\t{rstr}"""

from NDFSA import *
from myParser import *

def parse(pattern, text, **kwargs):
    config = kwargs.keys()
    parser = Parser(pattern = pattern)
    tree = parser.parse()

    if("fileName" in config):
        name = kwargs["fileName"]

    if("showTree" in config and kwargs["showTree"]):
        tree.generateImage(f"{name}tree")

    if("optimizeFSA" in config and kwargs["optimizeFSA"]):
        optimize = True

    class subFSATracker:
        def __init__(self, toState, symbol, fromState):
            self.toState = toState
            self.symbol = symbol
            self.fromState = fromState

    def convert(root, machine):
        if(root.left):
            leftSubMachine = convert(root.left, machine)
        if(root.right):
            rightSubMachine = convert(root.right, machine)

        if(root.token.name in ["CHAR", "ALIAS"]):
            #If the token is an alias, expand the alias.
            if(root.token.name == "ALIAS"):
                transitionValue = ALIASES[root.token.value[1:]]
            else:
                transitionValue = root.token.value

            machine.alphabet += transitionValue

            n = machine.addState(convert.id)

            directory = subFSATracker(n, transitionValue, n)
            
            convert.id += 1
        elif(root.token.name == "STAR"):
            n = machine.addState(convert.id)

            machine.addDelta(n, leftSubMachine.symbol, leftSubMachine.toState)
            machine.addDelta(leftSubMachine.fromState, 'Ɛ', n)

            directory = subFSATracker(n, 'Ɛ', n)
            convert.id += 1

        elif(root.token.name == "CONCAT"):
            if(optimize and root.right.token.name == "STAR" and root.left.token.name == "CHAR"):
                directory = subFSATracker(rightSubMachine.toState, leftSubMachine.symbol, rightSubMachine.fromState)
                machine.removeState(leftSubMachine.toState)
            else:
                directory = subFSATracker(leftSubMachine.toState, leftSubMachine.symbol, rightSubMachine.fromState)
                machine.addDelta(leftSubMachine.fromState, rightSubMachine.symbol, rightSubMachine.toState)

        elif(root.token.name == "PIPE"):
            n = machine.addState(convert.id)
            m = machine.addState(convert.id + 1)
            
            machine.addDelta(n, leftSubMachine.symbol, leftSubMachine.toState)
            machine.addDelta(n, rightSubMachine.symbol, rightSubMachine.toState)
            machine.addDelta(leftSubMachine.fromState, 'Ɛ', m)
            machine.addDelta(rightSubMachine.fromState, 'Ɛ', m)

            directory = subFSATracker(n, 'Ɛ', m)
            convert.id += 2

        return directory
    
    convert.id = 0

    #Build fsa from tree
    fsa = NDFSM()

    result = convert(tree, fsa)
    
    if(result.symbol == 'Ɛ'):
        fsa.start = result.toState
    else:
        fsa.start = fsa.addState('S')
        fsa.addDelta(fsa.start , result.symbol, result.toState)
        


    result.fromState.isFinal = True

    if("showFSA" in config and kwargs['showFSA']):
        fsa.generateImage(f"{name}fsa")

    return fsa.execute(text)