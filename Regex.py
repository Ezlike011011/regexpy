from NDFSA import *
from myParser import *

def parse(pattern, text, **kwargs):
    config = kwargs.keys()
    parser = Parser(pattern = pattern)
    tree = parser.parse()

    if("fileName" in config):
        name = kwargs["fileName"]

    if("showTree" in config and kwargs["showTree"]):
        tree.generateImage(f"{name}tree")

    if("optimizeFSA" in config and kwargs["optimizeFSA"]):
        optimize = True

    class subFSATracker:
        def __init__(self, toState, symbol, fromState):
            self.toState = toState
            self.symbol = symbol
            self.fromState = fromState

    def convert(root, machine):
        if(root.left):
            leftSubMachine = convert(root.left, machine)
        if(root.right):
            rightSubMachine = convert(root.right, machine)

        if(root.token.name in ["CHAR", "ALIAS"]):
            #If the token is an alias, expand the alias.
            if(root.token.name == "ALIAS"):
                transitionValue = ALIASES[root.token.value[1:]]
            else:
                transitionValue = root.token.value

            machine.alphabet += transitionValue

            n = machine.addState(convert.id)

            directory = subFSATracker(n, transitionValue, n)
            
            convert.id += 1
        elif(root.token.name == "STAR"):
            n = machine.addState(convert.id)

            machine.addDelta(n, leftSubMachine.symbol, leftSubMachine.toState)
            machine.addDelta(leftSubMachine.fromState, 'Ɛ', n)

            directory = subFSATracker(n, 'Ɛ', n)
            convert.id += 1

        elif(root.token.name == "CONCAT"):
            if(optimize and root.right.token.name == "STAR" and root.left.token.name == "CHAR"):
                directory = subFSATracker(rightSubMachine.toState, leftSubMachine.symbol, rightSubMachine.fromState)
                machine.removeState(leftSubMachine.toState)
            else:
                directory = subFSATracker(leftSubMachine.toState, leftSubMachine.symbol, rightSubMachine.fromState)
                machine.addDelta(leftSubMachine.fromState, rightSubMachine.symbol, rightSubMachine.toState)

        elif(root.token.name == "PIPE"):
            n = machine.addState(convert.id)
            m = machine.addState(convert.id + 1)
            
            machine.addDelta(n, leftSubMachine.symbol, leftSubMachine.toState)
            machine.addDelta(n, rightSubMachine.symbol, rightSubMachine.toState)
            machine.addDelta(leftSubMachine.fromState, 'Ɛ', m)
            machine.addDelta(rightSubMachine.fromState, 'Ɛ', m)

            directory = subFSATracker(n, 'Ɛ', m)
            convert.id += 2

        return directory
    
    convert.id = 0

    #Build fsa from tree
    fsa = NDFSM()

    result = convert(tree, fsa)
    
    if(result.symbol == 'Ɛ'):
        fsa.start = result.toState
    else:
        fsa.start = fsa.addState('S')
        fsa.addDelta(fsa.start , result.symbol, result.toState)
        
    result.fromState.isFinal = True

    if("showFSA" in config and kwargs['showFSA']):
        fsa.generateImage(f"{name}fsa")

    return fsa.execute(text)

if (__name__ == "__main__"):
    print(parse(r"(ab|d*)e*", r"", showTree = True, showFSA = True, fileName = "/output/pipe", optimizeFSA = True))
